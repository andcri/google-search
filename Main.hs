{-# LANGUAGE OverloadedStrings #-}

module Main where

import Lib
import Network.HTTP.Conduit
import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.Text as T
import System.IO
import Text.HTML.Scalpel
import Text.StringLike
import Data.List
import Control.Arrow (first)


t :: Scraper String [(String, String)]
t = chroots ("div" @: [hasClass "uUPGi"]) $ do
  link  <- attr "href" "a"
  title <- text $ "div" @: [ hasClass "AP7Wnd" ]
  return $ (title, link)


is_youtube xs = case get_prefix "youtube" xs of
                  Nothing -> False
                  Just _  -> True

fix_youtube :: String -> String
fix_youtube xs = T.unpack $ T.replace (T.pack "%3Fv%3D") (T.pack "?v=") (T.pack xs)

prefixesAndSuffixes :: [a] -> [([a],[a])]
prefixesAndSuffixes a =
    case a of
      [] -> [([], [])]
      (a : r) -> ([], a : r) : map (first (a:)) (prefixesAndSuffixes r)

get_prefix :: String -> String -> Maybe String
get_prefix start (x : xs) = case stripPrefix start (x : xs) of
                             Nothing -> get_prefix start xs
                             Just r  -> Just r
get_prefix _ [] = Nothing

-- pass a reversed string to get the suffix
get_suffix :: String -> String
get_suffix ('=' : 'a' : 's': '&' : xs) = reverse xs
get_suffix (x : xs) = get_suffix xs
get_suffix [] = ""


fix_links :: String -> String
fix_links xs = case get_prefix "q=" xs of
                 Nothing -> xs
                 Just r  -> if is_youtube r
                            then fix_youtube r
                            else get_suffix $ reverse r


display :: [(String, String)] -> IO ()
display []       = putStrLn ""
display (x : xs) = do
  putStrLn "=-=-=-=-=-="
  putStrLn (fst x)
  putStrLn (fix_links $ snd x)
  putStrLn "=-=-=-=-=-="
  putStrLn ""
  display xs


main = do
  putStrLn "===================="
  putStrLn "| Type your search |"
  putStrLn "===================="
  input <- getLine
  test <- scrapeURL ("https://www.google.com/search?hl=en&q=" ++ input) t
  case test of
    Nothing -> return ()
    Just r  -> do
      putStrLn "Results: "
      putStrLn ""
      display r
